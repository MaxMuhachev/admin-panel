<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Book_model
 * Модель для работы с книгами
 */
class Book_model extends CI_Model {

	/** 
	*Загрузка списка книг
	*/
	public function __construct()
    {
        $this->load->database();
    }

	public function get_book()
	{
	    $query = $this->db->get('book');
	    $res = $query->result_array();
	    return(json_encode($res));
	}

	public function get_response(&$date){
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->load->model('Book_model');
			$postData = file_get_contents('php://input');
			$data = json_decode($postData, true);
			$date = (int)$data['book_year'].substr(0, 4);//копируем год
			//проверка корректности даты 
			if (isset($data['book_year'])) {
				if ($date > (int)date('Y') || ($date == 0)) {
					show_error('Введите корректный год издания', 500, $heading = 'Произошла Ошибка');
				};
			}
			return $data;
		}
		else{
			return 0;
		}
	}

	public function insert($data, $date){
		$sql = "INSERT INTO `book` (`book_name`, `author_name`, `book_year`) 
			VALUES (".$this->db->escape($data['book_name']).","
			.$this->db->escape($data['author_name']).",'"
			.$this->db->escape_str($data['book_year'])."')";

		return $this->db->query($sql);
	}

	public function update($data, $date){
		$sql = "UPDATE `book` SET ";

		if (isset($data['book_name'])) {
			$sql = $sql."`book_name`= ".$this->db->escape($data['book_name']).", ";
		}
		if (isset($data['author_name'])) {
			$sql = $sql."`author_name`= ".$this->db->escape($data['author_name']).", ";
		}
		$sql = $sql."`book_year`= ".$this->db->escape($date);
		$sql = $sql." WHERE `book_id` = '".$this->db->escape_str($data['id'])."'";
		
		return $this->db->query($sql);
	}

	public function delete($data){
	    $sql = "DELETE FROM `book` WHERE `book_id` = ".$data['id'];
		return $this->db->query($sql);	
	}

	//получение книг в качестве объектов для xml заполнения
	public function get_book_object(){

	    $query = $this->db->get('book');
	    return($query->result_array());
	}

}
