<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Book
 * Контроллер для работы с книгами
 */
class Book extends CI_Controller {

	/**
	 * Загрузка списка книг
	 */

	public function loadList()
	{
		$this->load->model('Book_model');
		$bookList = $this->Book_model->get_book();
		echo $bookList;		
	}

	public function insert_book()
	{
		$this->load->model('Book_model');
		$date = "";
		$data = $this->Book_model->get_response($date);

		if ($data !== 0) {
			//если добавляем данные и поля не пустые
			if (($data['book_name'] !== "") && ($data['author_name'] !== "") && ($data['book_year'] !== null)) {
				$data = array(
			        'book_name' => $data['book_name'],
			        'author_name' =>$data['author_name'],
			        'book_year' => $date
			    );
				$this->Book_model->insert($data,$date);
				return 'Ok';
			}
			else{
				show_error('Введите все данные!', 500, $heading = 'Произошла Ошибка');
			};
		}
		else{
			//обращение кем-то не POST методом 
		}
	}

	public function update_book()
	{
		$this->load->model('Book_model');
		$date = "";
		$data = $this->Book_model->get_response($date);

		if ($data !== 0) {
			//если поля book_year, author_name, book_name не пустые
			if ((isset($data['book_year']) && ($data['book_year'] == null))) {
				show_error('Введите год издания!', 500, $heading = 'Произошла Ошибка');
			}
			elseif (isset($data['author_name']) && ($data['author_name'] == null)) {
				show_error('Введите автора книги!', 500, $heading = 'Произошла Ошибка');
			}
			elseif (isset($data['book_name']) && ($data['book_name'] == null)) {
				show_error('Введите название книги!', 500, $heading = 'Произошла Ошибка');
			}
			else{
				// изменяем данные
				$this->Book_model->update($data, $date);
				return 'Ok';
			}
		}
		else{
			//обращение кем-то не POST методом 
		}
	}

	public function delete_book()
	{
		$this->load->model('Book_model');
		$data = $this->Book_model->get_response($date);
		$this->Book_model->delete($data);
	}

	public function xmlbook(){

		$this->load->model('Book_model');
		$bookList = $this->Book_model->get_book_object();
		
		$xml = new domDocument("1.0", "utf-8");
		$sorts = $xml->appendChild($xml->createElement('books'));
		foreach ($bookList as $key) {
			$sort = $sorts->appendChild($xml->createElement('book'));
				$id = $sort->appendChild($xml->createElement('id'));
				$id->appendChild($xml->createTextNode($key['book_id']));
				$name = $sort->appendChild($xml->createElement('name'));
				$name->appendChild($xml->createTextNode($key['book_name']));
				$author = $sort->appendChild($xml->createElement('author'));
				$author->appendChild($xml->createTextNode($key['author_name']));
		}
		$xml->formatOutput = true;
		$xml->save('books.xml');
	}
}
