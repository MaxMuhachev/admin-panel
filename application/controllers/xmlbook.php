<?php 
	$this->load->model('Book_model');
	$bookList = $this->Book_model->get_book();
	
	$xml = new XMLWriter(); //создаем новый экземпляр класса XMLWriter
	$xml->openMemory(); //использование памяти для вывода строки
	
	foreach ($bookList as $key) {
		$xml->startDocument(); //установка версии XML в первом теге документа
		$xml->startElement("books");
		$xml->startElement("book");
		$xml->writeElement("id", $key['book_id']);
		$xml->writeElement("name", $key['book_name']);
		$xml->writeElement("author", $key['author_name']);
		$xml->endElement();
	}
	$xml->endElement();
	echo $xml->outputMemory();
 ?>