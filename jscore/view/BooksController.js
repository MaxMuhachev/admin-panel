/**Контроллер для Swan.view.Books*/
Ext.define('Swan.view.BooksController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.book',

	onInsertButtonClick(){
		const record = this.getView().getStore().getModel().create();//создаём запись в хранилище Store
		this.createBookDialog(record);
	},

	onUpdateButtonClick(){ 
		
		const selection = this.getView().getSelection();//получение выбранной записи в таблице
		this.createBookDialog(selection[0]);
	},

	onDeleteButtonClick() {
		const deleted = this.getView().getSelection(); //получение выбранной записи в таблице
		const names = deleted.map(record=>record.get('book_name')).join(', '); //получение название книги
		Ext.Msg.show({
			title: 'Удалить?',
			message: `Удалить книгу "${names}"?`,
			buttons: Ext.Msg.YESNO,
			icon: Ext.Msg.QUESTION,
			fn: button => {
				if (button === 'yes') {
					this.getView().getStore().proxy.url = 'index.php/Book/delete_book';
					this.delete(deleted);
				}
			}
		})
	},

	onXmlButtonClick() {
		Ext.onReady(function(){
		    Ext.Ajax.request({
		        url: 'index.php/Book/xmlbook',
		        success: function(response, options){
		            Ext.Msg.alert('Экспорт в XML','Xml текст в файле books.xml!')
		        }
		    }); 
		});
	},

	//для уменьшения количества кода
	createBookDialog(record){
		Ext.create({
			xtype: 'BookDialogWindow',
			autoShow: true,
			record: record,
			listeners: {
				submit: 'onBookDialogWindowSubmit',
				scope: this
			}
		});
	},
	onBookDialogWindowSubmit(bookDialogWindow, record){
		store.proxy.url= 'index.php/Book/setbook';
		const action = record.phantom ? 'insert' : 'update';
		this.showWaitWindow(action);
		this[action](bookDialogWindow, record).then(() => {
			this.closeWaitWindow();
			bookDialogWindow.close();
		this.getView().getStore().proxy.url = 'index.php/Book/loadList';
		}).catch(err => {
			this.closeWaitWindow();
			Ext.Msg.alert(err.message);
			store.remove(record);
			console.dir(err);
		});
	},

	insert(bookDialogWindow, record){
		this.getView().getStore().proxy.url = 'index.php/Book/insert_book';
		store = this.getView().getStore();
		store.add(record);
		record.data.id = undefined;
		return new Ext.Promise((resolve, reject) =>{
			store.sync({
				success: resolve,
				failure(batch, options) {
					const err = new Error('Произошла ошибка добавления');
					err.batch = batch;
					err.options = options;
					reject(err,);
				}
			});
		});
	},

	update(bookDialogWindow, record){
		this.getView().getStore().proxy.url = 'index.php/Book/update_book';
		store = this.getView().getStore();
		store.add(record);
		return new Ext.Promise((resolve, reject) =>{
			store.sync({
				success: resolve,
				failure(batch, options) {
					const err = new Error('Произошла ошибка обновления');
					err.batch = batch;
					err.options = options;
					reject(err);
				}
			});
		});
	},

	delete(record){
		
		this.getView().getStore().remove(record);
		record[0].id = record[0].data.book_id;
		this.getView().getStore().sync();
		this.getView().getStore().proxy.url = 'index.php/Book/loadList';
	},

	showWaitWindow(action){
		Ext.Msg.wait(
			`Данные ${{insert: 'добавляются', update: 'обновляются'}[action]}! 	Подождите..`,
			{insert: 'Добавление', update: 'Обновление'}[action]
		);
	},

	closeWaitWindow(){
		Ext.Msg.close();
	},

	onSelectChange(grid, selected){
		this.getViewModel().set('hasSelect', !selected.length);
	}
});