/*
*Контроллер для формы добавления и редактирования
*/
Ext.define('Swan.view.BookDialog.BookDialogController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.BookDialogWindow',

	// кнопка сохранить
	onSumbitButtonClick() {
		this.getView().down('form').updateRecord();
		this.fireViewEvent('submit', this.getView().getRecord());
	},

	//кнопка отмена
	onCancelButtonClick() {
		this.getView().close();
	},

	//после заполнение записей при нажатии редактировать
	afterRender(BookDialogView){
		this.getView().down('form').loadRecord(BookDialogView.getRecord())
	}
})