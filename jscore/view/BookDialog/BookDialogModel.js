/*
*Модель для формы редактирования
*/

Ext.define('Swan.view.BookDialog.BookDialogModel',{
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.BookDialogWindow',

	data: {
		record: null,
	},

	formulas: {
		title(get){
			return get('record').phantom ? 'Новая книга' : 'редактирование данных';
		},
		submitbuttonText(get){
			return get('record').phantom ? 'Добавить' : 'Обновить';
		}
	}
});
