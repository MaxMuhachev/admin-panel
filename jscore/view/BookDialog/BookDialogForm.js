/*
*Форма для добавления книг
*/
Ext.define('Swan.view.BookDialog.BookDialogForm', {
	extend: 'Ext.form.Panel',

	xtype: 'BookDialogForm',

	layout: 'anchor',
	bodyPadding: 10, //отступы

	//для всех textfield
	defaults: {
		anchor: '100%'
	},

	//для всех field потомков
	fieldDefaults: {
		labelalign: top,
		margin: 0,
		padding: '10'
	},

	items:[{
		fieldLabel: 'Название книги',
		xtype: 'textfield',
		allowBlank: false,
		name: 'book_name'
	}, {
		fieldLabel: 'Автор',
		xtype: 'textfield',
		allowBlank: false,
		name: 'author_name'
	}, {
		fieldLabel: 'Год издания',
		xtype: 'datefield',
		allowBlank: false,
		name: 'book_year',
		format: 'Y',
	    maxValue: new Date()
	}],

	buttons: [{
		text: 'Сохранить',
		handler: 'onSumbitButtonClick',
		bind: {
			text: '{submitbuttonText}'
		}
	},{
		text: 'Отмена',
		handler: 'onCancelButtonClick'
	}]
});