/*
*Окно для добавления книг
*/
Ext.define('Swan.view.BookDialog.BookDialog',{
	extend: 'Ext.window.Window',
	//короткое имя
	xtype: 'BookDialogWindow',

	controller: 'BookDialogWindow',
	viewModel: 'BookDialogWindow',
	//зависимость
	requires: [
		'Swan.view.BookDialog.BookDialogController',
		'Swan.view.BookDialog.BookDialogModel',
		'Swan.view.BookDialog.BookDialogForm'
	],

	/*
	* событие: submit
	* параметр1 BookDialog
	* параметр2 record
	*/

	with: 400,
	modal: true, //модальное окно
	resizable: false, //нелья менять размеры мышкой

	config: {
		record: null
	},

	bind: {
		title: '{title}'
	},
	
	//при нажатии редактировать копирует поля из таблицы
	updateRecord(record) {
		this.getViewModel().set('record', record.phantom ? record : record.copy())
		record.data.id = record.data.book_id;
	},

	items: {
		xtype: 'BookDialogForm'
	}

});