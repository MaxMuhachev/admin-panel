/*
*Хранилище для Swan.view.Books
*/
var store = Ext.create('Ext.data.Store',  {
		proxy: {
			type: 'ajax',
			url: 'index.php/Book/loadList',
			reader: {
				type: 'json',
				idProperty: 'book_id'
			}
		},
   		fields: ['book_id', 'book_name','author_name', 'book_year'],// список полей
		autoLoad: true,
		autoSync: false,
		sorters: [{
			property: 'book_name',
			direction: 'ASC'
		}]
});



var xmlstore = Ext.create('Ext.data.Store',  {
		proxy: {
			type: 'ajax',
	        url : 'book.xml',
	        reader: {
	        	type: 'xml',
	            record: 'book',
	            rootProperty: 'books'
	        }
	    }
});


Ext.define('Store', {
    extend: 'Ext.data.Model',
    fields: ['id_book','book_name','author_name', 'book_year'],// список полей
    validations: [{// валидаторы
        type: 'length',
        field: 'author_name', // поле город должно быть непустым
        min: 1
     }]
});

/**
 * Список книг
 */
Ext.define('Swan.view.Books', {
	extend: 'Ext.grid.Panel',
	xtype: 'BookList',

	controller: 'book',
	viewModel: 'booksmodel',

	requires: [
		'Swan.view.BooksController',
		'Ext.Promise',
		'Swan.view.BookDialog.BookDialog',
		'Swan.view.BooksModel'
	],

	store: store,
	tbar: [{
		text: 'Добавить',
		handler: 'onInsertButtonClick'
	},{
		text: 'Редактировать',
		handler: 'onUpdateButtonClick',
		bind:{
			disabled: '{!hasSelect}'
		}
	},{
		text: 'Удалить',
		handler: 'onDeleteButtonClick',
		bind:{
			disabled: '{!hasSelect}'
		}
	}, {
		text: 'Экспорт в XML',
		handler: 'onXmlButtonClick'
	}],

	listeners: {
		select: 'onSelectChange'
	},

	columns: [
		{
			dataIndex: 'book_name',
			text: 'Название книги',
			flex: 1
		}, {
			dataIndex: 'author_name',
			text: 'Автор',
			width: 150
		}, {
			dataIndex: 'book_year',
			text: 'Год издания',
			xtype:'datecolumn',	
			format: 'Y',
			width: 150

	    }
	]
});

