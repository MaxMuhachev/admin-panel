/*
*Модель для класса books
*/

Ext.define('Swan.view.BooksModel',{
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.booksmodel',

	data: {
		selection: null,
		hasSelect: false
	}
});